package uz.pdp.telegramwebhookapi.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import uz.pdp.telegramwebhookapi.payload.ResultMessage;
import uz.pdp.telegramwebhookapi.util.Utils;

@Service
@FeignClient(url = Utils.TELEGRAM_BASE_URL_WITHOUT_BOT,name = "Feign")
public interface TelegramFeign {

    @PostMapping("{path}/sendMessage")
    ResultMessage sendMessageToUser(@RequestParam String path, @RequestBody SendMessage sendMessage);

}
